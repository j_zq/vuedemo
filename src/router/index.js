import Vue from 'vue'
import Router from 'vue-router'

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import HelloWorld from '@/components/HelloWorld'
import Demo from '@/components/Demo'


Vue.use(ElementUI);
Vue.use(Router)
export default new Router({
  routes: [
    {
      path: '/',
      name: 'Demo',
      component: Demo
    }
  ]
})
